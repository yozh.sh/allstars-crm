from pyramid.view import view_config
from cornice import Service
from pyramid.httpexceptions import HTTPUnauthorized
from utils.response import JsonResponse, ErrorHttpJsonResponse
from marshmallow import ValidationError
from ...models.user_account.user import User
from ...models.user_account.schema import UserAuthSchema


auth = Service(name='auth', pyramid_route='auth')


@auth.post()
def login(request):
    schema = UserAuthSchema()
    try:
        schema.load(request.json_body)
    except ValidationError as er:
        return JsonResponse(json=er.messages, status=400)
    user = request.dbsession.query(User).filter_by(email=request.json_body['email']).first()
    if user:
        if user.check_password(request.json_body['password']):
            return JsonResponse(json={'token': request.create_jwt_token(user.id)})
    return ErrorHttpJsonResponse(HTTPUnauthorized)