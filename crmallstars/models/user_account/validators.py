from marshmallow import ValidationError

def password_length(password):
    if len(password) < 4:
        raise ValidationError('password must be equal or greater 4')