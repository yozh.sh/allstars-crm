from marshmallow import (
    Schema,
    fields
)
from .validators import password_length

class UserAuthSchema(Schema):
    email = fields.Email(required=True)
    password = fields.String(required=True, validate=password_length)