from ..meta import Base
from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    Boolean,
    ForeignKey
)
import bcrypt


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    departament_id = Column(Integer, ForeignKey('departaments.id'))
    firstname = Column(Unicode, nullable=False)
    lastname = Column(Unicode, nullable=False)
    email = Column(Unicode, nullable=False, unique=True)
    is_active = Column(Boolean, default=False)
    password_hash = Column(Unicode(60), nullable=False)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, plaintext_password):
        if len(plaintext_password) <= 4:
            raise ValueError('password must be contains 4 or more symbols not {}'.format(len(plaintext_password)))
        self.password_hash = bcrypt.hashpw(plaintext_password, bcrypt.gensalt())

    def check_password(self, plaintext_password):
        return bcrypt.checkpw(plaintext_password.encode('utf-8'), self.password_hash.encode('utf-8'))

    def __repr__(self):
        return self.email
