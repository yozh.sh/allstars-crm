from ..meta import Base

from sqlalchemy import (
    Column,
    Integer,
    Unicode,
)
from sqlalchemy.orm import relationship


class Departament(Base):
    __tablename__ = 'departaments'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False, unique=True)
    users = relationship("User")

    def __repr__(self):
        return self.name
