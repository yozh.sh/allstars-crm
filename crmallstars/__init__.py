from pyramid.config import Configurator
from routes import includeme
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.request import (
    Response,
    Request
)

def json_request_factory(environ):
    environ['HTTP_ACCEPT'] = 'application/json'
    request = Request(environ)
    request.response = Response()
    request.add_response_callback(finish_callback)
    return request

def finish_callback(peticion, respuesta):
    respuesta.headerlist.extend(
        (
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Headers', 'WWW-Authorization'),
            ('Access-Control-Allow-Headers', 'Content-Type'),
            ('Content-Type', 'application/json')
        )
    )
    return respuesta


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.set_request_factory(json_request_factory)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.include('pyramid_jwt')
    config.set_jwt_authentication_policy('top_SEEcret', http_header='JWT')
    config.include(includeme, route_prefix='/api/v2.0')
    config.include('cornice')
    config.include('.models')
    config.include('.routes')
    config.scan()
    return config.make_wsgi_app()
