"""add user and departament, initial

Revision ID: c186e8518017
Revises: 
Create Date: 2019-03-20 21:19:35.613124

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c186e8518017'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('departaments',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Unicode(), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_departaments')),
    sa.UniqueConstraint('name', name=op.f('uq_departaments_name'))
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('departament_id', sa.Integer(), nullable=True),
    sa.Column('firstname', sa.Unicode(), nullable=False),
    sa.Column('lastname', sa.Unicode(), nullable=False),
    sa.Column('email', sa.Unicode(), nullable=False),
    sa.Column('is_active', sa.Boolean(), nullable=True),
    sa.Column('password_hash', sa.Unicode(length=60), nullable=False),
    sa.ForeignKeyConstraint(['departament_id'], ['departaments.id'], name=op.f('fk_users_departament_id_departaments')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_users')),
    sa.UniqueConstraint('email', name=op.f('uq_users_email'))
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    op.drop_table('departaments')
    # ### end Alembic commands ###
