from pyramid.response import Response
from pyramid.httpexceptions import HTTPUnauthorized


def JsonResponse(json=dict(), status=200):
    data = {"data": json}
    return Response(json_body=data, status=status)


def ErrorHttpJsonResponse(HttpExc):
    exc = HttpExc()
    data = {"data": {"code": exc.status, "title": exc.title}}
    return data
